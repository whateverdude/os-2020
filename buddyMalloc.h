#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "buddyAllocator.c"

//buddyAllocator buddy;


void buddyMalloc_init(int totalLevels, char* memory, int memory_size, int minBucketSize, char* buffer, int buffer_size);

void* buddyMalloc_malloc(int size);

void* buddyMalloc_mallocPrint(int size);

void buddyMalloc_free(void* address);

void buddyMalloc_freePrint(void* address);
