#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "buddyMalloc.c"

#define MAX_BUFFER_SIZE 1048576    //max size of a file allowed and also max bucket size allowed
#define MAX_NODES 32768            //max node allowed, corresponding 16 levels(also counting the root(level 0) and the leaves(level 15))

int main(){
	
	printf("\n\n-------------------STARTING THE BUDDY ALLOCATOR----------------------\n\n");
	
	char printBuffers = 'a';
	char printBitmaps = 'a';
	char printInfos = 'a';

	int maxBucketSize,minBucketSize,minAllowed;
	int maxSize = 1,minSize = -1,totalLevels = 2;

	/*srand(time(NULL));

	int r1 = rand();
	int r2 = rand();
	int r3 = rand();
	int r4 = rand();

	int randomArray[4] = {r1,r2,r3,r4};
	int randomArray2[4] = {r1,r2,r3,r4}; //5%,15%,30%,50%
	
	printf("r1: %d \n", r1);
	printf("r2: %d \n", r2);
	printf("r3: %d \n", r3);
	printf("r4: %d \n", r4);

	while(1){
		
		int aux;		
		
		if(randomArray[0] > randomArray[1]){
			
			aux = randomArray[0];
			randomArray[0] = randomArray[1];
			randomArray[1] = aux;
		}

		if(randomArray[1] > randomArray[2]){
			
			aux = randomArray[1];
			randomArray[1] = randomArray[2];
			randomArray[2] = aux;
		}

		if(randomArray[2] > randomArray[3]){
			
			aux = randomArray[2];
			randomArray[2] = randomArray[3];
			randomArray[3] = aux;
		}

		if(randomArray[0]<randomArray[1] && randomArray[1]<randomArray[2] && randomArray[2]<randomArray[3]) break;
	
	}

	printf("\n");

	printf("r1: %d \n", randomArray[0]);
	printf("r2: %d \n", randomArray[1]);
	printf("r3: %d \n", randomArray[2]);
	printf("r4: %d \n", randomArray[3]);

	*/
	
	//asking the user for the biggest file to be stored
	while(maxSize > MAX_BUFFER_SIZE || maxSize<128){
	printf("Select the Maximum Size of a file you want to allocate.\nMin Allowed is 128 Bytes and Max Allowed is 1.048.576 Bytes)\n");
	scanf("%d",&maxSize);
	printf("\n");
	}
	
	//printf("maxSize: %d\n", maxSize);	

	//given the maximum file size, we calculate the maximum bucket size we need
	maxBucketSize = buddyAllocator_sizeToBucketN(maxSize, MAX_BUFFER_SIZE);

	printf("Max Bucket Size = %d Bytes\n\n", maxBucketSize);
	
	//since 16 levels are the most levels allowed, we calculate what the smallest bucket size can be, given the biggest
	minAllowed = (maxBucketSize>>15);
	if(minAllowed == 0) minAllowed = 2;
	
	//asking the user for the smallest file to be stored, minimum allowed was calculated in the step before
	while(minSize<minAllowed || minSize>(maxBucketSize>>2)){
	printf("Select the Minimum Size of a file you want to allocate.\nMin Allowed is %d Bytes and Max Allowed is %d Bytes\n", 		minAllowed, (maxBucketSize>>2));
	scanf("%d",&minSize);
	printf("\n");
	}
	
	//given the minimum file size, we calculate the minimum bucket size we need
	minBucketSize = buddyAllocator_sizeToBucketN(minSize, maxBucketSize);

	printf("Min Bucket Size = %d Bytes\n\n", minBucketSize);
	

	if((maxBucketSize/minBucketSize) > MAX_NODES) totalLevels = 16;
	else totalLevels = floor(log2(maxBucketSize/minBucketSize)) + 1;
	printf("Number of Levels = %d\n\n", totalLevels);

	while(printBuffers != 'Y' && printBuffers != 'y' && printBuffers != 'N' && printBuffers != 'n'){

	printf("Do you want to have the addresses printed(This won't affect the timings) (Y/N)?\n");
	printf("WARNING: DO NOT SELECT YES IF YOU SELECTED MORE THAN 1024 BYTES FOR YOU MAX BUCKET SIZE\n");
	scanf(" %c",&printBuffers);
	printf("\n");
	
	}

	
	while(printInfos != 'Y' && printInfos != 'y' && printInfos != 'N' && printInfos != 'n'){

	
	printf("Do you want to have additional infos printed?(This WILL affect the timings) (Y/N)\n");
	printf("WARNING: DO NOT SELECT YES IF NUMBER OF LEVELS IS BIGGER THAN 6\n");

	scanf(" %c",&printInfos);
	printf("\n");
	}
	
	//printf("printInfos: %c\n", printInfos);

	while(printBitmaps != 'Y' && printBitmaps != 'y' && printBitmaps != 'N' && printBitmaps != 'n'){

	printf("Do you want to have the Bitmaps printed?(This won't affect the timings) (Y/N)\n");
	printf("WARNING: DO NOT SELECT YES IF NUMBER OF LEVELS IS BIGGER THAN 6\n");
	scanf(" %c",&printBitmaps);

	printf("\n");
	}

	//printf("printBitmaps: %c\n", printBitmaps);

	//printing infos before we start
	printf("----------------------------PRINTING INFOS---------------------------\n\n");
	printf("---------------------------------------------------------------------\n\n");
	printf("Max Bucket Size = %d Bytes\n", maxBucketSize);
	printf("Min Bucket Size = %d Bytes\n", minBucketSize);
	printf("\n---------------------------------------------------------------------\n\n");
	printf("Memory needed for storage = %d Bytes\n", maxBucketSize);
	
	/*READ ME:
	//
	//Two different bitmaps are used to index the memory:
	//1) the indexBitmap, in which bits are set to 0 if available for being requested for memory and 1 otherwise
	//2) the statusBitmap, in which bits are set to 1 if they have been detached for memory, and 0 if they are not
	//Given the particular structure of a buddy allocator, a bit can be set to 1 in indexBitmap and still being 0 
	//in the statusBitmap
	//
	//Example: if we have maxBucketSize = 64 Bytes and minBucketSize = 16 Bytes we will have a total of 3 levels: 0,1,2;
	//this means e have a total of 7 nodes: 0,1,2,3,4,5,6
	//
	//LEVEL 0                              0                       exist at start---->[             64 Bytes             ]
	//                                    / \
	//                                   /   \
	//LEVEL 1                           1     2            both !(exist at start)---->[    32 Bytes    ][    32 Bytes    ]
	//                                 / \   / \
	//                                /   \ /   \
	//LEVEL 2                        3    4 5    6        all 4 !(exist at start)---->[16Bytes][16Bytes][16Bytes][16Bytes]
	//
	//
	//
	//TRADITIONAL BUDDY VIEW:          [             64 Bytes             ]
	//
	//
	//uint8_t* inside indexBitmap:   bits  [ 6 | 5 | 4 | 3 | 2 | 1 | 0 ]
	//                              values [ 0 | 0 | 0 | 0 | 0 | 0 | 0 ]                   
	//
	//uint8_t* inside statusBitmap:	 bits  [ 6 | 5 | 4 | 3 | 2 | 1 | 0 ]
	//                              values [ 0 | 0 | 0 | 0 | 0 | 0 | 0 ]              
	//
	//if we ask the buddy for a slot o 32 Bytes of memory(or any value < 33 and > 16), since all the space is free,
	//it will dismantle the 64Bytes block(corresponding to node 0 or root)
	//in 2 blocks(corresponding to node 1 and 2) and return us node 1,
	//since it's the first block of the right size we need.
	//The new setting is going to be something like this:
	//
	//
	//
	//LEVEL 0               !(exist)(has been divided)---->[             64 Bytes             ]
	//                                  
	//                                  
	//LEVEL 1                       exist and detached---->[////32 Bytes////][    32 Bytes    ]<----exist but not 
	//                                                                                                      detached  nor divided
	//                        
	//LEVEL 2                     the first 2 !(exist)---->[16Bytes][16Bytes][16Bytes][16Bytes]<---- the last 2 !(exist) because father
	//                       (because father is detached)                                               exist but is not divided,but
	//											     they can be created if father is split
	//
	//TRADITIONAL BUDDY VIEW:           [////32 Bytes////][    32 Bytes    ]
	//
	//
	//This means, that both the index and the status value of bit 1 is going to be set to 1.
	//Also the index value of bits 0,3 and 4 is going to be set to 1, meaning even they have not been detached they still
	//can't be used for being called to occupy memory, but their status value bits is going to stay at 0, meaning they can't be taken
	//but they have not been detached either.
	//
	//
	//uint8_t* inside indexBitmap:   bits  [ 6 | 5 | 4 | 3 | 2 | 1 | 0 ]
	//                              values [ 0 | 0 | 1 | 1 | 0 | 1 | 1 ]                   
	//
	//uint8_t* inside statusBitmap:	 bits  [ 6 | 5 | 4 | 3 | 2 | 1 | 0 ]
	//                              values [ 0 | 0 | 0 | 0 | 0 | 1 | 0 ]        
	//
	*/      
	
	

	//printing other infos before we start

	int memory_buffer_size;

	if(totalLevels<6) memory_buffer_size = 24;
	else memory_buffer_size = ((1<<(totalLevels - 3))*3);

	printf("Memory needed to store the tree = %d Bytes\n", memory_buffer_size);
	printf("Total Memory needed = %d Bytes\n", maxBucketSize + memory_buffer_size);
	printf("\n---------------------------------------------------------------------\n\n");
	printf("Number of Levels = %d\n", totalLevels);
	printf("Number of indexes/bits = %d\n", (1<<totalLevels) - 1);
	printf("indexBitmap, partialBitmap and statusBitmap uint64_t BITMAPs size = %d Bytes\n", memory_buffer_size/3);
	printf("\n---------------------------------------------------------------------\n\n");
	
	char memory[maxBucketSize];

	char buffer[memory_buffer_size];
	
	if(printBuffers == 'Y' || printBuffers == 'y'){

	printf("\n\n--------------------PRINTING MEMORY BYTE BY BYTE---------------------\n\n");
	for(int i = 0; i < maxBucketSize; i++){
		
		printf("memory[%3d]:  address:%p \n",i, memory+i);	
		if(i == ((maxBucketSize>>1) - 1)) printf("------------------------------------\n");

	}
	
	printf("\n\n--------------------PRINTING BUFFER BYTE BY BYTE---------------------\n\n");
	for(int i = 0; i < memory_buffer_size; i++){
	
		printf("buffer[%3d]:  address:%p \n",i, buffer+i);	
		if(i == (memory_buffer_size/3) - 1) printf("------------------------------------\n");
		if(i == (memory_buffer_size/3)*2 - 1) printf("------------------------------------\n");
		

	}

	printf("\n\n");
	}
	//initializing the buddy
	buddyMalloc_init(totalLevels, memory, maxBucketSize, minBucketSize, buffer, memory_buffer_size);
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);	
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	//TEST NUMBER 1
	//ALLOCATING ALL MEMORY WITH MINIMUM SIZED BUCKETS, THEN TRYING TO INSERT MORE BUCKETS, THEN FREEING THE MEMORY
	

	printf("\n--------------------------------------------TEST NUMBER 1--------------------------------------------\n");
	
	printf("\n                    ALLOCATING THE MEMORY WITH %d FILES OF SIZE: %d Bytes (minBucketSize)\n", 
	1<<(totalLevels - 1), minBucketSize);
	printf("\n                         THEN TRYING TO ADD %d MORE FILES OF SIZE %d, THEN FREEING THE MEMORY\n", 
	1<<(totalLevels - 1), minBucketSize);
	printf("\n-------------------------------------------ENTERING TEST 1-----------------------------------------\n\n");
	
	void* pointersArray[1<<totalLevels];
	//void* pointersArray2[1<<totalLevels];

	clock_t begin11;
	clock_t end11;
	
	if(printInfos == 'N' || printInfos == 'n'){
		
	begin11 = clock();	

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_malloc(minBucketSize);
			pointersArray[x-1] = myPointers;
			
		}
	
	end11 = clock();

	}else{
	
	begin11 = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_mallocPrint(minBucketSize);
			pointersArray[x-1] = myPointers;
			
		}

	end11 = clock();	

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
/*
	clock_t begin12;
	clock_t end12;	
	
	if(printInfos == 'N' || printInfos == 'n'){	

		begin12 = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_malloc(minBucketSize);
			pointersArray2[x-1] = myPointers;
		} 

		end12 = clock();
	
	}else{
		
		begin12 = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_mallocPrint(minBucketSize);
			pointersArray2[x-1] = myPointers;
		} 

		end12 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
*/
	clock_t begin13;
	clock_t end13;
	
	if(printInfos == 'N' || printInfos == 'n'){
	
		begin13 = clock();	

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			buddyMalloc_free(pointersArray[x-1]);

		}

		end13 = clock();
	
	}else{

		begin13 = clock();
		
		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			buddyMalloc_freePrint(pointersArray[x-1]);

		}

		end13 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
/*
	clock_t begin14;
	clock_t end14;
	
	if(printInfos == 'N' || printInfos == 'n'){

		begin14 = clock();		

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			buddyMalloc_free(pointersArray2[x-1]);

		}

		end14 = clock();

	}else{
		
		begin14 = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			buddyMalloc_freePrint(pointersArray2[x-1]);

		}

		end14 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
*/
	clock_t begin11_normal = clock();
	
	for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
		void* myPointers = malloc(minBucketSize);
		pointersArray[x-1] = myPointers;
	}

	clock_t end11_normal = clock();
/*	
	for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
		void* myPointers = malloc(minBucketSize);
		pointersArray2[x-1] = myPointers;
	} 
*/
	clock_t begin12_normal = clock();

	for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
		free(pointersArray[x-1]);

	}

	clock_t end12_normal = clock();
/*
	for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
		free(pointersArray2[x-1]);

	}
*/	

	printf("\n-------------------------------------------EXITING TEST 1-----------------------------------------\n\n");

	printf("\n--------------------------------------------TEST NUMBER 1A-------------------------------------------\n");
	
	printf("\n                    ALLOCATING THE MEMORY WITH %d FILES OF SIZE: %d Bytes (minBucketSize)\n", 
	1<<(totalLevels - 1), minBucketSize);
	printf("\n                 THEN TRYING TO ADD %d MORE FILES OF SIZE %d, THEN FREEING THE MEMORY BACKWARDS\n", 
	1<<(totalLevels - 1), minBucketSize);
	printf("\n-------------------------------------------ENTERING TEST 1A----------------------------------------\n\n");
	
	//void* pointersArray[1<<totalLevels];
	//void* pointersArray2[1<<totalLevels];

	clock_t begin11a;
	clock_t end11a;
	
	if(printInfos == 'N' || printInfos == 'n'){
		
	begin11a = clock();	

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_malloc(minBucketSize);
			pointersArray[x-1] = myPointers;
			
		}
	
	end11a = clock();

	}else{
	
	begin11a = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_mallocPrint(minBucketSize);
			pointersArray[x-1] = myPointers;
			
		}

	end11a = clock();	

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
/*
	clock_t begin12a;
	clock_t end12a;

	//void* pointersArray2[1<<totalLevels];	
	
	if(printInfos == 'N' || printInfos == 'n'){	

		begin12a = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_malloc(minBucketSize);
			pointersArray2[x-1] = myPointers;
		} 

		end12a = clock();
	
	}else{
		
		begin12a = clock();

		for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
			void* myPointers = buddyMalloc_mallocPrint(minBucketSize);
			pointersArray2[x-1] = myPointers;
		} 

		end12a = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
*/
	clock_t begin13a;
	clock_t end13a;
	
	if(printInfos == 'N' || printInfos == 'n'){
	
		begin13a = clock();	

		for(int x = 1<<(totalLevels - 1); x > 0; x--){
		
			buddyMalloc_free(pointersArray[x-1]);

		}

		end13a = clock();
	
	}else{

		begin13a = clock();
		
		for(int x = 1<<(totalLevels - 1); x > 0; x--){
		
			buddyMalloc_freePrint(pointersArray[x-1]);

		}

		end13a = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
/*
	clock_t begin14a;
	clock_t end14a;
	
	if(printInfos == 'N' || printInfos == 'n'){

		begin14a = clock();		

		for(int x = 1<<(totalLevels - 1); x > 0; x--){
		
			buddyMalloc_free(pointersArray2[x-1]);

		}

		end14a = clock();

	}else{
		
		begin14a = clock();

		for(int x = 1<<(totalLevels - 1); x > 0; x--){
		
			buddyMalloc_freePrint(pointersArray2[x-1]);

		}

		end14a = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
*/
	clock_t begin11a_normal = clock();
	
	for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
		void* myPointers = malloc(minBucketSize);
		pointersArray[x-1] = myPointers;
	}

	clock_t end11a_normal = clock();
/*
	for(int x = 1; x <= 1<<(totalLevels - 1); x++){
		
		void* myPointers = malloc(minBucketSize);
		pointersArray2[x-1] = myPointers;
	} 
*/	
	clock_t begin12a_normal = clock();
		
	for(int x = 1<<(totalLevels - 1); x > 0; x--){
		
		free(pointersArray[x-1]);

	}
/*
	for(int x = 1<<(totalLevels - 1); x > 0; x--){
		
		free(pointersArray2[x-1]);

	}
*/	
	clock_t end12a_normal = clock();

	printf("\n-------------------------------------------EXITING TEST 1A-----------------------------------------\n\n");



	//TEST NUMBER 2
	//ALLOCATING ALL MEMORY WITH ONE MAXIMUM SIZED BUCKET(JUST ONE CAN BE STORED AS IT TAKES THE WHOLE MEMORY),
	//THEN TRYING TO INSERT ANOTHER MAXIMUM SIZED BUCKET, THEN FREEING THE MEMORY
	
	printf("\n--------------------------------------------TEST NUMBER 2--------------------------------------------\n");
	printf("\n                    ALLOCATING ALL MEMORY WITH 1 SINGLE FILE OF SIZE: %d Bytes (maxBucketSize)\n", 
	maxBucketSize);
	printf("\n              THEN TRYING TO INSERT ANOTHER FILE OF SIZE %d (maxBucketSize), THEN FREEING THE MEMORY\n", 
	maxBucketSize);
	printf("\n-------------------------------------------ENTERING TEST 2-----------------------------------------\n\n");
	
	void* pointerAux, 
	    //*pointerAux2,
            *pointerAux3;

	clock_t begin21;
	clock_t end21;	
	
	if(printInfos == 'N' || printInfos == 'n'){

		begin21 = clock();
	
		void* myPointersAux = buddyMalloc_malloc(maxBucketSize);
		pointerAux = myPointersAux;

		end21 = clock();

	}else{
		
		begin21 = clock();

		void* myPointersAux = buddyMalloc_mallocPrint(maxBucketSize);
		pointerAux = myPointersAux;

		end21 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	clock_t begin23;
	clock_t end23;
	
	if(printInfos == 'N' || printInfos == 'n'){
		
		begin23 = clock();

		buddyMalloc_free(pointerAux);

		end23 = clock();
	
	}else{
		
		begin23 = clock();		

		buddyMalloc_freePrint(pointerAux);

		end23 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	clock_t begin21a = clock();

	void* myPointersAux3 = malloc(maxBucketSize);
	pointerAux3 = myPointersAux3;

	clock_t end21a = clock();

	clock_t begin22a = clock();

	free(pointerAux3);
	
	clock_t end22a = clock();	

	printf("\n-------------------------------------------EXITING TEST 2------------------------------------------\n\n");
	
	//TEST NUMBER 3
	//ALLOCATING ALL MEMORY WITH PROGRESSIVELY BIGGER FILES,
	//FROM MINIMUM TO MAXIMUM SIZE
	//RESULTS VARY DEPENDING ON THE LEVELS, BUT LAST FILE SHOULD ALWAYS FAIL MALLOC
	printf("\n--------------------------------------------TEST NUMBER 3--------------------------------------------\n");
	printf("\n                       ALLOCATING ALL MEMORY WITH PROGRESSIVELY BIGGER FILES\n");
	printf("\n                FROM A SIZE OF %d Bytes (minBucketSize), TO %d Bytes (maxBucketSize)\n",minBucketSize,
	 maxBucketSize);
	printf("\n                                      THEN FREEING THE MEMORY\n");
	printf("\n-------------------------------------------ENTERING TEST 3-----------------------------------------\n\n");
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	void* pointersArray6[1<<totalLevels];
	void* pointersArray6a[1<<totalLevels];

	clock_t begin51;
	clock_t end51;	
	
	if(printInfos == 'N' || printInfos == 'n'){

		begin51 = clock();		

			for(int x = 1; x < totalLevels + 1; x++){
		
			void* myPointers = buddyMalloc_malloc(minBucketSize*(1<<(x - 1)));
			pointersArray6[x-1] = myPointers;

		}

		end51 = clock();

	}else{
	
		begin51 = clock();

		for(int x = 1; x < totalLevels + 1; x++){
		
			void* myPointers = buddyMalloc_mallocPrint(minBucketSize*(1<<(x - 1)));
			pointersArray6[x-1] = myPointers;

		}

		end51 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	clock_t begin52;
	clock_t end52;
	
	if(printInfos == 'N' || printInfos == 'n'){

		begin52 = clock();

		for(int x = 1; x < totalLevels + 1; x++){
		
			buddyMalloc_free(pointersArray6[x-1]);

		}

		end52 = clock();

	}else{

		begin52 = clock();

		for(int x = 1; x < totalLevels + 1; x++){
		
			buddyMalloc_freePrint(pointersArray6[x-1]);

		}
	
		end52 = clock();

	}
		
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	clock_t begin51a = clock();	
	
	for(int x = 1; x < totalLevels + 1; x++){
		
		void* myPointers = malloc(minBucketSize*(1<<(x - 1)));
		pointersArray6a[x-1] = myPointers;

	}
	
	clock_t end51a = clock();

	clock_t begin52a = clock();

	for(int x = 1; x < totalLevels + 1; x++){
		
		free(pointersArray6a[x-1]);

	}
		
	clock_t end52a = clock();

	printf("\n-------------------------------------------EXITING TEST 3-----------------------------------------\n\n");

	//TEST NUMBER 4
	//ALLOCATING ALL MEMORY WITH PROGRESSIVELY SMALLER FILES,
	//FROM MAXIMUM TO MINIMUM SIZE
	//ONLY THE FIRST MALLOC SHOULD SUCCEED
	
	printf("\n--------------------------------------------TEST NUMBER 4--------------------------------------------\n");
	printf("\n                       ALLOCATING ALL MEMORY WITH PROGRESSIVELY SMALLER FILES\n");
	printf("\n                FROM A SIZE OF %d Bytes (maxBucketSize/2), TO %d Bytes (minBucketSize)\n", maxBucketSize/2,
	minBucketSize);
	printf("\n                                      THEN FREEING THE MEMORY\n");
	printf("\n-------------------------------------------ENTERING TEST 4------------------------------------------\n\n");
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
	
	void* pointersArray7[1<<totalLevels];
	void* pointersArray7a[1<<totalLevels];

	clock_t begin61;
	clock_t end61;

	if(printInfos == 'N' || printInfos == 'n'){

		begin61 = clock();
	
		for(int x = 1; x < totalLevels; x++){
		
			void* myPointers = buddyMalloc_malloc(maxBucketSize/(1<<(x)));
			pointersArray7[x - 1] = myPointers;

		}

		end61 = clock();

	}else{
		
		begin61 = clock();

		for(int x = 1; x < totalLevels; x++){
		
			void* myPointers = buddyMalloc_mallocPrint(maxBucketSize/(1<<(x)));
			pointersArray7[x - 1] = myPointers;

		}

		end61 = clock();

	}
	
	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}

	clock_t begin62;
	clock_t end62;
	
	if(printInfos == 'N' || printInfos == 'n'){

		begin62 = clock();

		for(int x = 1; x < totalLevels; x++){
		
			buddyMalloc_free(pointersArray7[x - 1]);

		}

		end62 = clock();

	}else{
		
		begin62 = clock();

		for(int x = 1; x < totalLevels; x++){
		
			buddyMalloc_freePrint(pointersArray7[x - 1]);

		}

		end62 = clock();

	}

	if(printBitmaps == 'Y' || printBitmaps == 'y'){

	printf("\n----------------------------INDEX BITMAP-----------------------------\n\n");
	BitMap_printBits(&buddy.indexBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------PARTIAL BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.partialBitmap, (1<<totalLevels) - 2);
	printf("\n----------------------------STATUS BITMAP----------------------------\n\n");
	BitMap_printBits(&buddy.statusBitmap, (1<<totalLevels) - 2);
	printf("\n---------------------------------------------------------------------\n\n");
	
	}
	//
	clock_t begin61a = clock();

	for(int x = 1; x < totalLevels; x++){
		
		void* myPointers = malloc(maxBucketSize/(1<<(x)));
		pointersArray7a[x - 1] = myPointers;

	}

	clock_t end61a = clock();

	clock_t begin62a = clock();

	for(int x = 1; x < totalLevels; x++){
		
		free(pointersArray7a[x - 1]);

	}

	clock_t end62a = clock();
	

	printf("\n-------------------------------------------EXITING TEST 4------------------------------------------\n\n");

	//int randomicArray[100];
	//clock_t end = clock();
	
	double time_spent_test1 = (double)((end11 - begin11)+/*(end12 - begin12)*/+(end13 - begin13)/*+(end14 - begin14)*/);
	double time_spent_test11 = (double)(end11 - begin11);
	//double time_spent_test12 = (double)(end12 - begin12);
	double time_spent_test13 = (double)(end13 - begin13);
	//double time_spent_test14 = (double)(end14 - begin14);

	double time_spent_test1_normal = (double)((end11_normal - begin11_normal) + (end12_normal - begin12_normal));

	double time_spent_test11_normal = (double)(end11_normal - begin11_normal);
	double time_spent_test12_normal = (double)(end12_normal - begin12_normal);

	double time_spent_test1a = (double)((end11a - begin11a)+/*(end12a - begin12a)*/+(end13a - begin13a)/*+(end14a - begin14a)*/);
	double time_spent_test11a = (double)(end11a - begin11a);
	//double time_spent_test12a = (double)(end12a - begin12a);
	double time_spent_test13a = (double)(end13a - begin13a);
	//double time_spent_test14a = (double)(end14a - begin14a);

	double time_spent_test1a_normal = (double)((end11a_normal - begin11a_normal) + (end12a_normal - begin12a_normal));

	double time_spent_test11a_normal = (double)(end11a_normal - begin11a_normal);
	double time_spent_test12a_normal = (double)(end12a_normal - begin12a_normal);

	double time_spent_test2 = (double)((end21 - begin21)+/*(end22 - begin22)+*/(end23 - begin23)/*+(end24 - begin24)*/);
	double time_spent_test2a = (double)((end21a - begin21a)+(end22a - begin22a));

	double time_spent_test5 = (double)((end51 - begin51)+(end52 - begin52));
	double time_spent_test5a = (double)((end51a - begin51a)+(end52a - begin52a));

	double time_spent_test6 = (double)((end61 - begin61)+(end62 - begin62));
	double time_spent_test6a = (double)((end61a - begin61a)+(end62a - begin62a));

	double time_spent_total = time_spent_test1 + time_spent_test2 + time_spent_test5 + time_spent_test6;	

	printf("\n\n\n\n--------------------------------------------PRINTING RESULTS--------------------------------------------\n");
	
	printf("\n\n--------------------------------------CLOCKS PER SECOND: %lf clocks per second\n\n", (double)CLOCKS_PER_SEC);

	printf("-----------------------------------TOTAL CLOCKS ELAPSED: %lf clock cycles\n\n", time_spent_total);

	printf("-------------------------------------TOTAL TIME ELAPSED: %lf seconds\n\n", time_spent_total/(double)CLOCKS_PER_SEC);


	printf("\n-----------------------------------------------TEST 1------------------------------------------------\n");
	
	printf("\n                    ALLOCATING THE MEMORY WITH %d FILES OF SIZE: %d Bytes (minBucketSize)\n", 
	1<<(totalLevels - 1), minBucketSize);
	printf("\n                         THEN TRYING TO ADD %d MORE FILES OF SIZE %d, THEN FREEING THE MEMORY\n", 
	1<<(totalLevels - 1), minBucketSize);

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n                                  CLOCKS ELAPSED TEST 1: %lf clock cycles\n", time_spent_test1);
	
	printf("                                    TIME ELAPSED TEST 1: %lf seconds \n\n", time_spent_test1/(double)CLOCKS_PER_SEC);

	printf("                      CLOCKS ELAPSED TEST 1 with malloc: %lf clock cycles\n", time_spent_test1_normal);
	
	printf("                        TIME ELAPSED TEST 1 with malloc: %lf seconds\n\n", time_spent_test1_normal/(double)CLOCKS_PER_SEC);

	printf("\n-----------------------------------------------------------------------------------------------------\n");
	
	printf("\n---------------------------CLOCKS ELAPSED TEST 1(Allocating): %lf clock cycles\n", time_spent_test11);
	
	printf("-----------------------------TIME ELAPSED TEST 1(Allocating): %lf seconds\n\n", time_spent_test11/(double)CLOCKS_PER_SEC);

	printf("------------------------------CLOCKS ELAPSED TEST 1(Freeing): %lf clock cycles\n", time_spent_test13);
	
	printf("--------------------------------TIME ELAPSED TEST 1(Freeing): %lf seconds\n\n", time_spent_test13/(double)CLOCKS_PER_SEC);

	printf("\n--------------CLOCKS ELAPSED TEST 1A with malloc(Allocating): %lf clock cycles\n", time_spent_test11_normal);
	
	printf("----------------TIME ELAPSED TEST 1A with malloc(Allocating): %lf seconds\n\n", time_spent_test11_normal/(double)CLOCKS_PER_SEC);

	printf("-----------------CLOCKS ELAPSED TEST 1A with malloc(Freeing): %lf clock cycles\n", time_spent_test12_normal);
	
	printf("-------------------TIME ELAPSED TEST 1A with malloc(Freeing): %lf seconds\n\n", time_spent_test12_normal/(double)CLOCKS_PER_SEC);

	
	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n-----------------------------------------------TEST 1A-----------------------------------------------\n");
	
	printf("\n                    ALLOCATING THE MEMORY WITH %d FILES OF SIZE: %d Bytes (minBucketSize)\n", 
	1<<(totalLevels - 1), minBucketSize);
	printf("\n                         THEN TRYING TO ADD %d MORE FILES OF SIZE %d, THEN FREEING THE MEMORY\n", 
	1<<(totalLevels - 1), minBucketSize);

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n                                 CLOCKS ELAPSED TEST 1A: %lf clock cycles\n", time_spent_test1a);
	
	printf("                                   TIME ELAPSED TEST 1A: %lf seconds \n\n", time_spent_test1a/(double)CLOCKS_PER_SEC);

	printf("                     CLOCKS ELAPSED TEST 1A with malloc: %lf clock cycles\n", time_spent_test1a_normal);
	
	printf("                       TIME ELAPSED TEST 1A with malloc: %lf seconds\n\n", time_spent_test1a_normal/(double)CLOCKS_PER_SEC);

	printf("\n-----------------------------------------------------------------------------------------------------\n");
	
	printf("\n--------------------------CLOCKS ELAPSED TEST 1A(Allocating): %lf clock cycles\n", time_spent_test11a);
	
	printf("----------------------------TIME ELAPSED TEST 1A(Allocating): %lf seconds\n\n", time_spent_test11a/(double)CLOCKS_PER_SEC);

	printf("-----------------------------CLOCKS ELAPSED TEST 1A(Freeing): %lf clock cycles\n", time_spent_test13a);
	
	printf("-------------------------------TIME ELAPSED TEST 1A(Freeing): %lf seconds\n\n", time_spent_test13a/(double)CLOCKS_PER_SEC);

	printf("\n--------------CLOCKS ELAPSED TEST 1A with malloc(Allocating): %lf clock cycles\n", time_spent_test11a_normal);
	
	printf("----------------TIME ELAPSED TEST 1A with malloc(Allocating): %lf seconds\n\n", time_spent_test11a_normal/(double)CLOCKS_PER_SEC);

	printf("-----------------CLOCKS ELAPSED TEST 1A with malloc(Freeing): %lf clock cycles\n", time_spent_test12a_normal);
	
	printf("-------------------TIME ELAPSED TEST 1A with malloc(Freeing): %lf seconds\n\n", time_spent_test12a_normal/(double)CLOCKS_PER_SEC);
	
	printf("\n-----------------------------------------------------------------------------------------------------\n");
	

	printf("\n-----------------------------------------------TEST 2------------------------------------------------\n");
	printf("\n                    ALLOCATING ALL MEMORY WITH 1 SINGLE FILE OF SIZE: %d Bytes (maxBucketSize)\n", 
	maxBucketSize);
	printf("\n              THEN TRYING TO INSERT ANOTHER FILE OF SIZE %d (maxBucketSize), THEN FREEING THE MEMORY\n", 
	maxBucketSize);

	printf("\n----------------------------------CLOCKS ELAPSED TEST 2: %lf clock cycles\n", time_spent_test2);
	
	printf("------------------------------------TIME ELAPSED TEST 2: %lf seconds\n\n", time_spent_test2/(double)CLOCKS_PER_SEC);

	
	printf("----------------------CLOCKS ELAPSED TEST 2 with malloc: %lf clock cycles\n", time_spent_test2a);
	
	printf("------------------------TIME ELAPSED TEST 2 with malloc: %lf seconds\n\n", time_spent_test2a/(double)CLOCKS_PER_SEC);

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	
	printf("\n-----------------------------------------------TEST 3------------------------------------------------\n");
	printf("\n                       ALLOCATING ALL MEMORY WITH PROGRESSIVELY BIGGER FILES\n");
	printf("\n                FROM A SIZE OF %d Bytes (minBucketSize), TO %d Bytes (maxBucketSize)\n",minBucketSize,
	 maxBucketSize);
	printf("\n                                      THEN FREEING THE MEMORY\n");
	
	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n----------------------------------CLOCKS ELAPSED TEST 3: %lf clock cycles\n", time_spent_test5);
	
	printf("------------------------------------TIME ELAPSED TEST 3: %lf seconds\n\n", time_spent_test5/(double)CLOCKS_PER_SEC);

	
	printf("----------------------CLOCKS ELAPSED TEST 3 with malloc: %lf clock cycles\n", time_spent_test5a);
	
	printf("------------------------TIME ELAPSED TEST 3 with malloc: %lf seconds\n\n", time_spent_test5a/(double)CLOCKS_PER_SEC);

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n-----------------------------------------------------------------------------------------------------\n");
	
	printf("\n-----------------------------------------------TEST 4------------------------------------------------\n");
	printf("\n                       ALLOCATING ALL MEMORY WITH PROGRESSIVELY SMALLER FILES\n");
	printf("\n                FROM A SIZE OF %d Bytes (maxBucketSize/2), TO %d Bytes (minBucketSize)\n", maxBucketSize/2,
	minBucketSize);
	printf("\n                                      THEN FREEING THE MEMORY\n");     

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n----------------------------------CLOCKS ELAPSED TEST 4: %lf clock cycles\n", time_spent_test6);
	
	printf("------------------------------------TIME ELAPSED TEST 4: %lf seconds\n\n", time_spent_test6/(double)CLOCKS_PER_SEC);

	
	printf("----------------------CLOCKS ELAPSED TEST 4 with malloc: %lf clock cycles\n", time_spent_test6a);
	
	printf("------------------------TIME ELAPSED TEST 4 with malloc: %lf seconds\n\n", time_spent_test6a/(double)CLOCKS_PER_SEC);

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n-----------------------------------------------------------------------------------------------------\n");

	printf("\n\n---------------------------------------QUITTING THE BUDDY ALLOCATOR----------------------------------\n\n");


}
