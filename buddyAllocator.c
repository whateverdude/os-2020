#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include "buddyAllocator.h"

//Set to 1 the nth bit in both the index and status bitmaps, then set to 1 all the children and parents of n in the index bitmap
void buddyAllocator_SetOne(BitMap* indexB, BitMap* statusB, BitMap* partialB, int n, int length, int level, int maxLevels){
	
	BitMap_setBit(partialB,n);
	BitMap_setBit(statusB,n);

	int min = n;
	int max = n;
	
	int aux;
	
	while(max < length){
		
		for(aux = min; aux <= max; aux++){
			
			BitMap_setBit(indexB,aux);
		}

		min = (min*2) + 1;
		max = (max*2) + 2;		
	}

	if(!(n==0)) BitMap_SetFatherOne(indexB, partialB, (n-1)/2);
}

//Set to 0 the nth bit in both the index and status bitmaps, then set to 0 all the children and parents of n in the index bitmap
void buddyAllocator_SetZero(BitMap* indexB, BitMap* statusB, BitMap* partialB, int n, int length, int level, int maxLevels){
	
       	BitMap_clearBit(partialB,n);
	BitMap_clearBit(statusB,n);

	int min = n;
	int max = n;
	
	int aux;
	
	while(max < length){
		
		for(aux = min; aux <= max; aux++){
		
			BitMap_clearBit(indexB,aux);
					
		}

		min = (min*2) + 1;
		max = (max*2) + 2;		
	}

	if(!(n==0)) BitMap_SetFatherZero(indexB, partialB, (n-1)/2);
}

//Malloc for files of a given size, gets informations from buddy then calls buddyAllocator_getBuddyy
void* buddyAllocator_getBuddy(buddyAllocator* buddy, int size){
	
	if(size>buddy->memory_size){
	
		//printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, REQUESTING TOO MUCH MEMORY ]\n");
		return NULL;	
		
	}
	
	if(size == 0){
	
		//printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, REQUESTING 0 BYTES OF MEMORY ]\n");
		return NULL;
	
	}
	
	if(BitMap_checkBit(&buddy->partialBitmap, 0) == 1){
	
		//printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, NOT ENOUGH FREE SPACE ]\n");
		return NULL;
	}

	if(BitMap_checkBit(&buddy->statusBitmap, 0) == 1){
	
		//printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, NOT ENOUGH FREE SPACE ]\n");
		return NULL;
	}
	
	int bucket;
	
	//given size, calculate the corresponding bucket
	if(size <= buddy->minBucketSize){ bucket = buddy->minBucketSize;}
	else{bucket = buddyAllocator_sizeToBucket(size, buddy->memory_size, buddy->levels);}

	//given a bucket, calculate the corresponding level
	int level = buddyAllocator_bucketToLevel(buddy->levels, buddy->minBucketSize, bucket);

	if(level>buddy->levels){
		
		//printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, REQUESTING TOO MUCH MEMORY ]\n");
		return NULL;
	}
	
	//given a level, calculate the first free index on that level(if there is any)
	//clock_t time3 = clock();
	
	int index = BitMap_computeIndexN(&buddy->partialBitmap, &buddy->indexBitmap, level);
	
	//clock_t time4 = clock();
		
	//printf("TIME COMPUTE INDEX = %lf\n", (double)(time4 - time3)/(CLOCKS_PER_SEC));

	//if no free index is found on that level we return -1
	if(index == -1){

		//printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, NOT ENOUGH FREE SPACE ]\n");		
		return NULL;	
	}
	//ask for the memory block pointed by the index level and set the Bitmap index and the Bitmap status
	return buddyAllocator_getBuddyy(buddy, index, level, bucket);
}

void* buddyAllocator_getBuddyPrint(buddyAllocator* buddy, int size){
	
	if(size>buddy->memory_size){
	
		printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, REQUESTING TOO MUCH MEMORY ]\n");
		return NULL;	
		
	}
	
	if(size == 0){
	
		printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, REQUESTING 0 BYTES OF MEMORY ]\n");
		return NULL;
	
	}
	
	if(BitMap_checkBit(&buddy->partialBitmap, 0) == 1){
	
		printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, NOT ENOUGH FREE SPACE ]\n");
		return NULL;
	}

	if(BitMap_checkBit(&buddy->statusBitmap, 0) == 1){
	
		printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, NOT ENOUGH FREE SPACE ]\n");
		return NULL;
	}
	
	int bucket;
	
	//given size, calculate the corresponding bucket
	if(size <= buddy->minBucketSize){ bucket = buddy->minBucketSize;}
	else{bucket = buddyAllocator_sizeToBucket(size, buddy->memory_size, buddy->levels);}

	//given a bucket, calculate the corresponding level
	int level = buddyAllocator_bucketToLevel(buddy->levels, buddy->minBucketSize, bucket);

	if(level>buddy->levels){
		
		printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, REQUESTING TOO MUCH MEMORY ]\n");
		return NULL;
	}
	
	//given a level, calculate the first free index on that level(if there is any)
	//clock_t time3 = clock();
	
	int index = BitMap_computeIndexN(&buddy->partialBitmap, &buddy->indexBitmap, level);
	
	//clock_t time4 = clock();
		
	//printf("TIME COMPUTE INDEX = %lf\n", (double)(time4 - time3)/(CLOCKS_PER_SEC));

	//if no free index is found on that level we return -1
	if(index == -1){

		printf("[ MALLOC UNSUCCESSFULL ] [ ERROR, NOT ENOUGH FREE SPACE ]\n");		
		return NULL;	
	}
	//ask for the memory block pointed by the index level and set the Bitmap index and the Bitmap status
	return buddyAllocator_getBuddyyPrint(buddy, index, level, bucket);
}	

//Malloc
void* buddyAllocator_getBuddyy(buddyAllocator* buddy, int n, int level, int bucket){	
		
	buddyAllocator_SetOne(&buddy->indexBitmap, &buddy->statusBitmap, &buddy->partialBitmap, n, buddy->bits, level, buddy->levels);

	return (buddy->memory + (n - ((1<<level) - 1))*bucket);
}

void* buddyAllocator_getBuddyyPrint(buddyAllocator* buddy, int n, int level, int bucket){	
		
	buddyAllocator_SetOne(&buddy->indexBitmap, &buddy->statusBitmap, &buddy->partialBitmap, n, buddy->bits, level, buddy->levels);
	
	printf("[ FILE OF SIZE: %3d] [ PLACED AT INDEX: %3d] [ ADDRESS: %3p]\n",
	bucket, n, (buddy->memory + (n - ((1<<level) - 1))*bucket));	

	return (buddy->memory + (n - ((1<<level) - 1))*bucket);
}

//Given a pointer, it tries to free the index linked to it. It calls the buddyAllocator_freeBuddyy function to do so
void buddyAllocator_freeBuddy(buddyAllocator* buddy, void* pointer){
	
	if(pointer == NULL){
	
		//printf("[ FREE UNSUCCESSFULL ] [ ERROR, YOU PASSED AN INVALID NULL POINTER ]\n");
		return;
	}
	//clock_t time1 = clock();
	int index = buddyAllocator_pointerToIndex(buddy, pointer);
	//clock_t time2 = clock();

	//printf("TIME POINTER TO INDEX = %lf\n", (double)(time2 - time1)/(CLOCKS_PER_SEC));
	
	buddyAllocator_freeBuddyy(buddy, index);

}

void buddyAllocator_freeBuddyPrint(buddyAllocator* buddy, void* pointer){
	
	if(pointer == NULL){
	
		printf("[ FREE UNSUCCESSFULL ] [ ERROR, YOU PASSED AN INVALID NULL POINTER ]\n");
		return;
	}
	//clock_t time1 = clock();
	int index = buddyAllocator_pointerToIndexPrint(buddy, pointer);
	//clock_t time2 = clock();

	//printf("TIME POINTER TO INDEX = %lf\n", (double)(time2 - time1)/(CLOCKS_PER_SEC));
	
	buddyAllocator_freeBuddyyPrint(buddy, index);

}

//Free
void buddyAllocator_freeBuddyy(buddyAllocator* buddy, int n){
		
	if(BitMap_checkBit(&buddy->statusBitmap, n) == 0){
		
		//printf("[ FREE UNSUCCESSFULL ] [ ERROR, NO BLOCK WAS LOADED ON THAT ADDRESS ]\n");
		return;
	
	}

	buddyAllocator_SetZero(&buddy->indexBitmap, &buddy->statusBitmap, &buddy->partialBitmap, n, buddy->bits, 			  		buddyAllocator_indexToLevel(n), buddy->levels);
}

void buddyAllocator_freeBuddyyPrint(buddyAllocator* buddy, int n){
		
	if(BitMap_checkBit(&buddy->statusBitmap, n) == 0){
		
		printf("[ FREE UNSUCCESSFULL ] [ ERROR, NO BLOCK WAS LOADED ON THAT ADDRESS ]\n");
		return;
	
	}

	buddyAllocator_SetZero(&buddy->indexBitmap, &buddy->statusBitmap, &buddy->partialBitmap, n, buddy->bits, 			  		buddyAllocator_indexToLevel(n), buddy->levels);
}

//Given size, calculate the corresponding bucket
int buddyAllocator_sizeToBucket(int size, int n, int levels){
	
	if(size <= n && size > n/2) return n;
	
	int divider = (1<<(levels/2));
	int middleBucketHigh = n/divider;
	int middleBucketLow = middleBucketHigh/2;

	while(!(size <= middleBucketHigh && size > middleBucketLow)){

		if(size > middleBucketHigh){
			
			middleBucketHigh = middleBucketHigh*(divider/2);
			middleBucketLow = middleBucketLow*(divider/2);
			divider /= 2;		

		}

		else if(size <= middleBucketLow){
			
			middleBucketHigh = middleBucketHigh/(divider/2);
			middleBucketLow = middleBucketLow/(divider/2);
			divider /= 2;
			
		}
	}

	return middleBucketHigh;

}

//BAD
int buddyAllocator_sizeToBucketN(int size, int n){
	
	if(size <= n && size > n/2) return n;
	//RECURSIVE VERSION
	//return(buddyAllocator_sizeToBucket(size,n/2));

	/*int middleBucketHigh = n/(1<<(levels/2));
	int middleBucketLow = middleBucketHigh/2;
	
	while(!(size <= middleBucketHigh && size > middleBucketLow)){

		if(size > middleBucketHigh){
			
			middleBucketHigh *=2;
			middleBucketLow *=2;
		}

		if(size <= middleBucketLow){
			
			middleBucketHigh /=2;
			middleBucketLow /=2;
		}
	}

	return middleBucketHigh;*/

	int min = n/2;

	while(!(size <= min*2 && size > min)){

		min = min/2;
	}

	return min*2;

}

//GOOD
//Given a bucket, calculate the corresponding level
int buddyAllocator_bucketToLevel(int levels, int minBucketSize, int bucket){
	
	return (levels-1)-(log2(bucket/minBucketSize));

}

//GOOD
//Given an index, calculate the level
int buddyAllocator_indexToLevel(int n){
		
	return (int)floor(log2(n+1));

}

//GOOD
//Given a level, calculate the bucket size for that level
int buddyAllocator_levelToBucket(int size, int level){

	return size/(1<<level);
}

//GOOD
//Given a pointer, calculate the index corresponding. Also check if that index on the status bitmap is detached
//if it is, and the pointer address is correct, it claims a successfull free
int buddyAllocator_pointerToIndex(buddyAllocator* buddy, void* pointer){
	
	int aux = buddy->memory_size;
	int pow = buddy->levels;
	
	for(int i = 0; i < pow; i++){

		if(((char*)pointer - buddy->memory)%aux == 0){
			
			int aux2 = ((char*)pointer - buddy->memory)/aux;
			
			int aux3 = buddy->startingLevelIndexes[i] + aux2;
				
			if(BitMap_checkBit(&buddy->statusBitmap, aux3)){

				//printf("[ FREE SUCCESSFULL ] [ ADDRESS:%p ] [ INDEX:%d ]\n", pointer, aux3);
	
				return aux3;
				break;
				
			}
	
		}	

	aux /= 2;	

	}

	return - 1;	
}

int buddyAllocator_pointerToIndexPrint(buddyAllocator* buddy, void* pointer){
	
	int aux = buddy->memory_size;
	int pow = buddy->levels;
	
	for(int i = 0; i < pow; i++){

		if(((char*)pointer - buddy->memory)%aux == 0){
			
			int aux2 = ((char*)pointer - buddy->memory)/aux;
			
			int aux3 = buddy->startingLevelIndexes[i] + aux2;
				
			if(BitMap_checkBit(&buddy->statusBitmap, aux3)){

				printf("[ FREE SUCCESSFULL ] [ ADDRESS:%p ] [ INDEX:%d ]\n", pointer, aux3);
	
				return aux3;
				break;
				
			}
	
		}	

	aux /= 2;	

	}

	return - 1;	
}

//Initialize the buddyAllocator
void buddyAllocatorInitialize(buddyAllocator* buddy, int levels, char* memory, int memory_size, int minBucketSize, char* buffer, int buffer_size){

	buddy->levels = levels;
	buddy->bits = (1<<levels) - 1;

	buddy->memory_size = memory_size;
	buddy->minBucketSize = minBucketSize;
	buddy->buffer_size = buffer_size;

	buddy->memory = memory;
	buddy->buffer = buffer;
	
	for(int i = 0; i < 16; i++){

		buddy->startingLevelIndexes[i] = (1<<i) - 1;
	}
	
	int aux = (buffer_size<<3)/3;

	BitMap_initialize(&buddy->indexBitmap, (uint64_t*)buffer, aux);
	BitMap_initialize(&buddy->statusBitmap, (uint64_t*)(buffer + (buffer_size/3)), aux);
	BitMap_initialize(&buddy->partialBitmap, (uint64_t*)(buffer + (buffer_size/3)*2), aux);
	
}

