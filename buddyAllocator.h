#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include "bitmap.c"

typedef struct buddyAllocator {

	BitMap indexBitmap;  //BitMap telling us if an index is available or not
	BitMap statusBitmap; //BitMap telling us if an index is detached or not
	BitMap partialBitmap; //BitMap telling us if an index has both children occupied

	int levels;          //The number of total levels of the tree
	int bits;            //The total number of bits used, aka the total number of nodes

	char* memory;        //memory used for storage
	int memory_size;     //size of all memory and also maximum bucket size
	int minBucketSize;   //minimum bucket size   

	char* buffer;        //memory used to store the BitMaps
	int buffer_size;     //size of the buffer
	
	int startingLevelIndexes[16]; //contains the starting index for each level

} buddyAllocator;

//Set to 1 the nth bit in both the index and status bitmaps, then set to 1 all the children and parents of n in the index bitmap
void buddyAllocator_SetOne(BitMap* indexB, BitMap* statusB, BitMap* partialB, int n, int length, int level, int maxLevels);

//Set to 0 the nth bit in both the index and status bitmaps, then set to 0 all the children and parents of n in the index bitmap
void buddyAllocator_SetZero(BitMap* indexB, BitMap* statusB, BitMap* partialB, int n, int length, int level, int maxLevels);

//Malloc for files of a given size, gets informations from buddy then calls buddyAllocator_getBuddyy
void* buddyAllocator_getBuddy(buddyAllocator* buddy, int size);

void* buddyAllocator_getBuddyPrint(buddyAllocator* buddy, int size);

//Malloc
void* buddyAllocator_getBuddyy(buddyAllocator* buddy, int n, int level, int bucket);

void* buddyAllocator_getBuddyyPrint(buddyAllocator* buddy, int n, int level, int bucket);

//Given a pointer, it tries to free the index linked to it. It calls the buddyAllocator_freeBuddyy function to do so
void buddyAllocator_freeBuddy(buddyAllocator* buddy, void* pointer);

void buddyAllocator_freeBuddyPrint(buddyAllocator* buddy, void* pointer);

//Free
void buddyAllocator_freeBuddyy(buddyAllocator* buddy, int n);

void buddyAllocator_freeBuddyyPrint(buddyAllocator* buddy, int n);

int buddyAllocator_sizeToBucketN(int size, int n);
//Given size, calculate the corresponding bucket
int buddyAllocator_sizeToBucket(int size, int n, int levels);

//Given a bucket, calculate the corresponding level
int buddyAllocator_bucketToLevel(int levels, int minBucketSize, int bucket);

//Given an index, calculate the level
int buddyAllocator_indexToLevel(int n);

//Given a level, calculate the bucket size for that level
int buddyAllocator_levelToBucket(int size, int level);

//Given a pointer, calculate the index corresponding. Also check if that index on the status bitmap is detached
//if it is, and the pointer address is correct, it claims a successfull free
int buddyAllocator_pointerToIndex(buddyAllocator* buddy, void* pointer);

int buddyAllocator_pointerToIndexPrint(buddyAllocator* buddy, void* pointer);

//Initialize the buddyAllocator
void buddyAllocatorInitialize(buddyAllocator* buddy, int levels, char* memory, int memory_size, int minBucketSize, char* buffer, int buffer_size);
