#include <stdint.h>

typedef struct BitMap {

	uint64_t* indexMatrix;
	
	short size;
	short bits;

} BitMap;

//checks the status of a bit(if it is either 1 or 0);
int BitMap_checkBit(BitMap* bitmap, int n);

//sets a bit to 1 
void BitMap_setBit(BitMap* bitmap, int n);

//sets a bit to 0
void BitMap_clearBit(BitMap* bitmap, int n);

//initialize the BitMap
void BitMap_initialize(BitMap* bitmap, uint64_t* matrix, short bits);

//print
void BitMap_printBits(BitMap* bitmap, int lenght);

//complementary function to set indexBitmap parents of index n to 1, when a malloc is called
void BitMap_SetFatherOne(BitMap* indexB, BitMap* partialB, int n);

//complementary function to set indexBitmap parents of index n to 0, when a free is called
void BitMap_SetFatherZero(BitMap* indexB, BitMap* partialB, int n);

int BitMap_computeIndex(BitMap* bitmap, int level);

int BitMap_computeIndexN(BitMap* partialBitmap, BitMap* indexBitmap, int level);

int BitMap_computeIndexR(BitMap* partialBitmap, BitMap* indexBitmap, int level, int n);

