# -*- MakeFile -*-

all: buddyMallocTest

buddyMallocTest: buddyMallocTest.o
	gcc buddyMallocTest.o -o buddyMallocTest -lm -Wall

buddyMallocTest.o: buddyMallocTest.c
	gcc -c buddyMallocTest.c -lm -Wall

clean:
	rm *.o buddyMallocTest
