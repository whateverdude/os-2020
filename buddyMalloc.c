#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include "buddyMalloc.h"

//declaring the buddy
buddyAllocator buddy;
	

void buddyMalloc_init(int totalLevels, char* memory, int memory_size, int minBucketSize, char* buffer, int buffer_size){

	//initializing the buddy
	buddyAllocatorInitialize(&buddy, totalLevels, memory, memory_size, minBucketSize, buffer, buffer_size);
	return;
}

void* buddyMalloc_malloc(int size){

	 return buddyAllocator_getBuddy(&buddy, size);

}

void* buddyMalloc_mallocPrint(int size){

	 return buddyAllocator_getBuddyPrint(&buddy, size);

}

void buddyMalloc_free(void* address){

	buddyAllocator_freeBuddy(&buddy, address);
	return;

}

void buddyMalloc_freePrint(void* address){

	buddyAllocator_freeBuddyPrint(&buddy, address);
	return;

}
