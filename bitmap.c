#include "bitmap.h"

//GOOD
//checks the status of a bit(if it is either 1 or 0);
int BitMap_checkBit(BitMap* bitmap, int n){
	
	if(bitmap->indexMatrix[n/64] & ((uint64_t)1<<(n%64))){

		return 1;
	}
	else return 0;		
	
}

//GOOD
//sets a bit to 1 
void BitMap_setBit(BitMap* bitmap, int n){
	
	bitmap->indexMatrix[n/64] |= ((uint64_t)1<<(n%64));

}

//GOOD
//sets a bit to 0
void BitMap_clearBit(BitMap* bitmap, int n){

	bitmap->indexMatrix[n/64] &= ~((uint64_t)1<<(n%64));
}

//GOOD
//initialize the BitMap
void BitMap_initialize(BitMap* bitmap, uint64_t* matrix, short bits){

	bitmap->indexMatrix = matrix;

	bitmap->size = bits/64;
	bitmap->bits = bits;

	for(int i = 0; i < bits/64; i++){
		
		*(matrix + i) = 0;

	}
	
}

//GOOD
//print
void BitMap_printBits(BitMap* bitmap, int lenght){
	
	printf("[");
        for(int j = lenght; j>=0; j--){
		
		if(j!=0)printf("%4d| ",j);
		else printf("%4d",j);

	}
	printf("]\n\n");	

	printf("[");
	for(int i = lenght; i>=0; i--){
		
		if(i!=0)printf("%4d, ", BitMap_checkBit(bitmap, i));
		else printf("%4d", BitMap_checkBit(bitmap, i));

	}
	printf("]\n\n");
}


//given a level, check for a free index on that level
int BitMap_computeIndex(BitMap* bitmap, int level){
	
	int aux = (1<<level) - 1;
	int aux2 = (1<<(level + 1)) - 1;

	for(int i = aux; i < aux2; i++){
		
		if(BitMap_checkBit(bitmap, i) == 0){
			
			return i;
			break;
			
		}
	}

	return -1;

}
//CHANGE THIS INTO ITERATIVE
int BitMap_computeIndexR(BitMap* partialBitmap, BitMap* indexBitmap, int level, int n){
	
	if(level == 0 && !BitMap_checkBit(indexBitmap,n)) return n;
	

	if(level > 1){

		if(!BitMap_checkBit(partialBitmap, (n*2) + 1))
		return BitMap_computeIndexR(partialBitmap, indexBitmap, level - 1, n*2 + 1);


		if(!BitMap_checkBit(partialBitmap, (n*2) + 2))
		return BitMap_computeIndexR(partialBitmap, indexBitmap, level - 1, n*2 + 2);

	}

	if(level == 1){
	
		if(!BitMap_checkBit(indexBitmap, (n*2) + 1))
		return BitMap_computeIndexR(partialBitmap, indexBitmap, level - 1, n*2 + 1);


		if(!BitMap_checkBit(indexBitmap, (n*2) + 2))
		return BitMap_computeIndexR(partialBitmap, indexBitmap, level - 1, n*2 + 2);

	}

	return - 1;

}

int BitMap_computeIndexN(BitMap* partialBitmap, BitMap* indexBitmap, int level){
	
	if(BitMap_checkBit(partialBitmap,0)) return -1;

	if(!level){

		if(BitMap_checkBit(indexBitmap,0)) return -1;
		else return 0;

	}
	
	int auxLevel = 0;

	int aux1 = 0;
	int token;

	while(1){
		
		token = 1;
		
		if(auxLevel + 1 == level){
			
			if(BitMap_checkBit(indexBitmap, aux1*2+1) && BitMap_checkBit(indexBitmap, aux1*2+2)) return - 1;
			if(!BitMap_checkBit(indexBitmap, aux1*2+1)) return aux1*2+1;
			if(!BitMap_checkBit(indexBitmap, aux1*2+2)) return aux1*2+2;
		}
		
		if(auxLevel + 1 != level){
			
			token = 0;
			
			if(!BitMap_checkBit(partialBitmap, aux1*2+1) && !token){
				
				aux1 = aux1*2+1;
				auxLevel++;
				token = 1;
				
			}	
			
			if(!BitMap_checkBit(partialBitmap, aux1*2+2) && !token){
				
				aux1 = aux1*2+2;
				auxLevel++;
				token = 1;
			}
		}
	
	
	}

	return -1;
	
}

//GOOD
//complementary function to set indexBitmap parents of index n to 1, when a malloc is called
void BitMap_SetFatherOne(BitMap* indexB, BitMap* partialB, int n){
	
	while(n != -1){
		
		BitMap_setBit(indexB,n);

		if(BitMap_checkBit(partialB, (n*2) + 1) && BitMap_checkBit(partialB, (n*2) + 2))
		BitMap_setBit(partialB,n);
		
		if(n == 0) n = -1;
		else n = (n-1)/2;
	}

}

//GOOD
//complementary function to set indexBitmap parents of index n to 0, when a free is called
void BitMap_SetFatherZero(BitMap* indexB, BitMap* partialB, int n){
	
	while(n != -1){
	
		if(!BitMap_checkBit(indexB, (n*2) + 1) && !BitMap_checkBit(indexB, (n*2) + 2))
		BitMap_clearBit(indexB,n);

		if(!BitMap_checkBit(partialB, (n*2) + 1) || !BitMap_checkBit(partialB, (n*2) + 2))
		BitMap_clearBit(partialB,n);	
		
		if(n == 0) n = -1;
		else n = (n-1)/2;

	}

}

